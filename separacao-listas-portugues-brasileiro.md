# Registro

**Mensagens do Felipe durante a reunião realizada no dia 31/08/2018 no canal
debian-l10n-br sobre a questão da separação das listas de português de Portugal
e do Brasil:**

<faw\> <https://lists.debian.org/debian-i18n/2005/10/msg00077.html>

<faw\> <https://lists.debian.org/debian-www/2011/08/msg00007.html>

<faw\> <https://lists.debian.org/debian-www/2011/07/msg00174.html>

<faw\> <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=362165>

<faw\> Melhor resultado seria tornar as listas pra refletir as comunidades e não os idiomas.

 * debian-news-brazilian
 * debian-l10n-brazilian
 * debian-user-brazilian
 * debian-devel-brazilian

-- não por orgulho ou algo assim, mas simplesmente por facilitar organização de encontros e eventos de forma mais geolocalizada, e se for o caso criar um super grupo de falantes de português, mas esse objetivo não é um caminho fácil

<faw\> tem bastante educação pra ser feita e ajustes e tem que ter amplo suporte. entre os times de tradução é extremamente simples, e as listas que são de baixo fluxo como de notícias, provavelmente são fáceis. As de desenvolvedores também devem ser simples, as de suporte são mais complicadas.

<faw\> Não é inviável, só dá trabalho :)  Tem nos arquivos a discussão da possibilidade e bastante gente é a favor, na verdade listmasters podem mudar isso nos bastidores

<faw\> Só precisa coordenar anúncios e esforço e ter amplo suporte de que isso é o certo a ser feito pra ambas as comunidades...

<faw\> Talvez a nova geração consiga fazer isso sem atear fogo em quem estiver tentando promover as mudanças ;-)

<faw\> Não sei se o Rui e o Miguel ainda traduzem, eles era a favor, faz sentido pra eles, eles não tem quase espaço oficial dentro da infra do Debian, não teria porque eles serem contra.

<faw\> Na verdade é mais uma questão de fazer a coisa certa do ponto de vista de quem tem o privilegio

<faw\> Uma confusão normal era achar que era algo separatista, então ter o suporte do time de Portugal é bom pra evitar essa confusão e mostrar que estão todos na mesma página

<faw\> Acabei atrapalhando a reunião, mas é que eu sempre acho curioso que as gerações de voluntários tendem a ignorar os esforços da geração anterior... foi assim nas gerações anteriores...

<faw\> adrianorg, provavelmente não... o desafio é sempre o mesmo, a geração anterior está tentando proteger certos itens, a geração nova quer avançar de forma diferente. Tme um artigo do Rand que trata disso: A velha e a nova guarda:
<http://randsinrepose.com/archives/the-old-guard>
